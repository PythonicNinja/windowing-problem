import Lines = require("../../helpers/lines");
import Nodes = require ("./node");


export interface IntervalNodeInterface {
    Xmed:number;
    Lleft:Lines.Line[];
    Lright:Lines.Line[];
}

export class IntervalNode extends Nodes.Node implements IntervalNodeInterface {

    left:IntervalNode;
    right:IntervalNode;
    Xmed:number;
    Lleft:Lines.Line[];
    Lright:Lines.Line[];

    constructor(left = null, right = null, data = null, Xmed = null) {
        super(arguments);
        this.left = left;
        this.right = right;
        this.data = data;
        this.Xmed = Xmed;
    }
}
