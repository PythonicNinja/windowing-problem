///<reference path="../../helpers/points.ts" />
import Region = require("../../helpers/region");
import Points = require("../../helpers/points");

    export interface NodeInterface {
        isLeaf(): boolean;
        preorder(): Node[];
        inorder(): Node[];
        postorder(): Node[];
        children(): Node[];
        reportsubtree(): Node[];
        set_child(index:number, child:Node): void;
        height(): number;
    }
    export class Node implements NodeInterface{
        left: Node;
        right: Node;
        data: Points.Point;
        constructor(left=null, right=null, data=null) {
            this.left = left;
            this.right = right;
            this.data = data;
        }
        toString(){
            return  this.left + '< ' + this.data + ' >' + this.right
        }

        isLeaf():boolean {
            return this.children().every(function(obj){return obj === null});
        }

        preorder():Node[] {
            var nodes = [];
            nodes.push(this);
            if(this.left) nodes.push(this.left.preorder());
            if(this.right) nodes.push(this.right.preorder());

            return nodes;
        }

        reportsubtree():Node[] {
            var nodes = [];
            if(this.isLeaf()) nodes.push(this);
            if(this.left) nodes = nodes.concat(this.left.reportsubtree());
            if(this.right) nodes = nodes.concat(this.right.reportsubtree());

            return nodes;
        }

        inorder():Node[] {
            return undefined;
        }

        postorder():Node[] {
            return undefined;
        }

        children():Node[] {
            return [this.left, this.right];
        }

        set_child(index:number, child:Node):void {
            return undefined;
        }

        height():number {
            return undefined;
        }

    }
