///<reference path="../../helpers/lines.ts" />
///<reference path="node.ts" />
import Region = require("../../helpers/region");
import Points = require("../../helpers/points");
import Nodes = require ("./node");

    export class KdNode extends Nodes.Node {
        region:Region.Region;
        left:KdNode;
        right:KdNode;
        data:Points.Point;

        constructor(left = null, right = null, data = null, region = null) {
            super(arguments);
            this.left = left;
            this.right = right;
            this.data = data;
            this.region = region;

            return this;
        }

        toString() {
            return this.left + '< ' + this.data + ' | ' + this.region + ' >' + this.right
        }

        fullyContains(R:Region.Region) {
            return this.region.l.x >= R.l.x &&
                this.region.l.y >= R.l.y &&
                this.region.r.x <= R.r.x &&
                this.region.r.y <= R.r.y;
        }
    }

