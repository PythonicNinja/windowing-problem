/**
 * Created by Jarek on 25.03.15.
 */

import IntervalNode = require("./nodes/IntervalNode");
import Lines = require("../helpers/lines");
import _ = require('lodash');


export interface IntervalTreeInterface {
    v:IntervalNode.IntervalNode;
}


export class IntervalTree implements IntervalTreeInterface {
    v:IntervalNode.IntervalNode;


    constructor(l:Lines.Line[]) {

        //PRIVATE !!!!
        var med = function (S:number[]):number {
            var mediana:number;
            var index:number;
            S.sort();
            if (S.length % 2 !== 0) {
                index = Math.floor((S.length + 1) / 2);
                mediana = S[index];
            } else {
                index = Math.floor((S.length - 1) / 2);
                mediana = (1 / 2) * (S[index] + S[index + 1]);
            }

            return mediana
        };
        var defineLleft = function (Smed:Lines.Line[]) {
            var temp = _.sortBy(Smed, function (n:Lines.Line) {
                return n.a.x;
            });
            return temp;

        };
        var defineLright = function (Smed:Lines.Line[]) {
            var temp = _.sortBy(Smed, function (n:Lines.Line) {
                return -1 * n.b.x;
            });
            return temp;
        };
        var defineSleft = function (xMed:number, l:Lines.Line[]) {
            var Sleft:Lines.Line[] = [];
            _.each(l, function (entry:Lines.Line) {
                if (entry.b.x < xMed) {
                    Sleft.push(entry);
                }
            });
            return Sleft;
        };
        var defineSright = function (xMed:number, l:Lines.Line[]) {
            var Sright:Lines.Line[] = [];
            _.each(l, function (entry:Lines.Line) {
                if (entry.a.x > xMed) {
                    Sright.push(entry);
                }
            });
            return Sright;
        };
        var defineSmed = function (xMed:number, l:Lines.Line[]) {
            var Smed:Lines.Line[] = [];
            _.each(l, function (entry:Lines.Line) {
                if (entry.a.x < xMed && entry.b.x >= xMed) {
                    Smed.push(entry);
                }
            });
            return Smed;
        };
        var getRangesFromLines = function (l) {
            var xCoordinates:number[] = [];
            _.each(l, function (entry:Lines.Line) {
                xCoordinates.push(entry.a.x);
                xCoordinates.push(entry.b.x);
            });

            return xCoordinates;
        };
        var ConstructIntervalTree = function (l):IntervalNode.IntervalNode {
            if (l.length === 0)
                return new IntervalNode.IntervalNode();
            else {
                var v = new IntervalNode.IntervalNode();
                v.Xmed = med(getRangesFromLines(l));
                //console.log("v.Xmed: ", v.Xmed);
                var Smed = defineSmed(v.Xmed, l);
                var Sleft = defineSleft(v.Xmed, l);
                var Sright = defineSright(v.Xmed, l);
                v.Lleft = defineLleft(Smed);
                v.Lright = defineLright(Smed);
                //console.log("v.Lleft", v.Lleft);
                //console.log("v.Lright", v.Lright);

                //console.log("Smed", defineSmed(v.Xmed, l));

                //Recursion starts here
                v.left = ConstructIntervalTree(Sleft);      // left son
                v.right = ConstructIntervalTree(Sright);    // right son
                return v;
            }
        };

        //PUBLIC
        this.v = ConstructIntervalTree(l);

    }

    QueryIntervalTree(v:IntervalNode.IntervalNode, l:Lines.Line) {

        var that = this;
        var foundLines:Lines.Line[] = [];

        if (v.left !== null || v.right !== null) {
            if (l.a.x <= v.Xmed) {
                _.each(v.Lleft, function (entry:Lines.Line) {
                    if (entry.a.x <= l.a.x && entry.a.y >= l.a.y && entry.b.y <= l.b.y) {
                        //console.log("found: ", entry);
                        foundLines.push(entry);
                    } else {
                        that.QueryIntervalTree(v.left, l);
                    }
                });
            } else {
                _.each(v.Lright, function (entry:Lines.Line) {
                    if (entry.b.x >= l.a.x && entry.a.y >= l.a.y && entry.b.y <= l.b.y) {
                        //console.log("found: ", entry);
                        foundLines.push(entry);
                    } else {
                        that.QueryIntervalTree(v.right, l);
                    }
                });
            }
        }

        return foundLines;
    }


}