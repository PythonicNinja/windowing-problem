///<reference path="nodes/kd_node.ts" />
///<reference path="../helpers/points.ts" />

import Nodes = require("./nodes/kd_node");
import Region = require ("../helpers/region");
import Points = require("../helpers/points");

interface KdTreeI {
        BuildKdTree(S: Points.Point[] , d:number): Nodes.KdNode;
        KdTreeQuery(v: Nodes.KdNode, R:any): Points.Point[];
    }

class KdTree implements KdTreeI {
        v: Nodes.KdNode;
        constructor(S:Points.Point[], d:number) {
            this.v = this.BuildKdTree(S, d);
        }

        BuildKdTree(S:Points.Point[], d:number):Nodes.KdNode {
            var input_length = S.length;
            if (input_length == 1) return new Nodes.KdNode(null, null, S[0], new Region.Region(S[0], S[0]));
            else if(d % 2 == 0) {
                var m = Math.floor(S.length/2);
                var l = new Points.Point(S[m].x, 0);
                var S1 = S.slice(0, m);
                var S2 = S.slice(m);
            }
            else{
                var m = Math.floor(S.length/2);
                var l = new Points.Point(0, S[m].y);
                var S1 = S.slice(0, m);
                var S2 = S.slice(m);
            }
            var Sx = S.sort(function(a, b){return a.x - b.x}).slice(0);
            var Sy = S.sort(function(a, b){return a.y - b.y}).slice(0);

            var r = new Region.Region(
                new Points.Point(Sx[0].x, Sy[0].y),
                new Points.Point(Sx[Sx.length - 1].x, Sy[Sy.length - 1].y)
            );

            return new Nodes.KdNode(this.BuildKdTree(S1, d+1), this.BuildKdTree(S2, d+1), l, r);
        }

    KdTreeQuery(v:Nodes.KdNode, R:Region.Region):Points.Point[] {

            if(v.isLeaf()) return v.data.isInArea(R) ? [v.data] : [];
            else {
                var points = [];

                if(v.left.fullyContains(R))
                    points = points.concat(v.left.reportsubtree().map(function(obj){return obj.data}));
                else
                    points = points.concat(this.KdTreeQuery(v.left, R));


                if(v.right.fullyContains(R))
                    points = points.concat(v.right.reportsubtree().map(function (obj) {return obj.data}));
                else
                    points = points.concat(this.KdTreeQuery(v.right, R));

                return points;
            }
        }
    }
export = KdTree;
