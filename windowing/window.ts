///<reference path="../references.ts" />

import Region = require("../helpers/region");
import Points = require("../helpers/points");
import Lines = require("../helpers/lines");
import KdTree = require("../tries/kd_tree");
import IntervalTree = require("../tries/intervalTree");
import _ = require('lodash');

export interface WindowInterface {
    windowQuery(R:Region.Region):Lines.Line[];
}

export class Window implements WindowInterface {

    lines:Lines.Line[];
    region:Region.Region;
    points:Points.Point[];
    pointsInRegion:Points.Point[];

    constructor(l:Lines.Line[], r:Region.Region) {
        //PRIVATE !!!!
        var that = this;
        var convertLinesToPoints = function (lines:Lines.Line[]):Points.Point[] {
            var points:Points.Point[] = [];

            _.each(lines, function (entry, index) {
                entry.a.belongsToLine = index; //remember which line points belongs to
                entry.b.belongsToLine = index;
                points.push(entry.a);
                points.push(entry.b);
            });

            return points;
        };
        var findPointsInRegion = function (points:Points.Point[]) {
            var kdTree = new KdTree(points, 0);
            //console.log("findPointsInRegion@KdTreeQuery",kdTree.KdTreeQuery(kdTree.v, r));
            return kdTree.KdTreeQuery(kdTree.v, r);
        };

        //PUBLIC
        this.lines = l;
        this.points = convertLinesToPoints(l);
        this.pointsInRegion = findPointsInRegion(convertLinesToPoints(l));
    }


    windowQuery(R:Region.Region):Lines.Line[] {
        var foundLines:Lines.Line[] = [];
        var temp:boolean[] = []; //helper variable to store flags of lines that have been reported and avoid repeating
        var that = this; //just javascript


        //Left border of Region
        var l = new Lines.Line(R.l,
            new Points.Point(R.l.x, R.r.y));


        //FIRST WE CHECK LINES THAT HAVE AT LEAST ONE END IN GIVEN REGION
        _.each(this.pointsInRegion, function (entry) {
            if (temp[entry.belongsToLine] !== true) {
                foundLines.push(that.lines[entry.belongsToLine]);
                temp[entry.belongsToLine] = true;
            }
        });

        //Then we define horizontal lines that intersect left border of Region
        //we omit found lines before
        _.each(foundLines, function (entry) {
            _.pull(that.lines, entry); //remove founded lines from lines to search O(n) ?
        });


        var intervalTree:IntervalTree.IntervalTree = new IntervalTree.IntervalTree(this.lines);
        //console.log("intervalTree Xmed: ", intervalTree.v.Xmed);
        _.each(intervalTree.QueryIntervalTree(intervalTree.v, l), function (entry) {
            foundLines.push(entry);
        });


        return foundLines;
    }


}
