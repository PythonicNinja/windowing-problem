import Region = require("region");
export interface PointInterface {
    getDist(): number;
    isInArea(R:Region.Region): boolean;
}
export class Point implements PointInterface {
    x:number;
    y:number;
    belongsToLine:number;

    constructor(x:number, y:number) {
        this.x = x;
        this.y = y;
    }

    getDist() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    toString() {
        return "{" + this.x.toString() + ", " + this.y.toString() + "}";
    }

    isInArea(R):boolean {
        return R.l.x <= this.x
            && this.x <= R.r.x
            && R.l.y <= this.y
            && this.y <= R.r.y;
    }
}

