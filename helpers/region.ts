import Points = require("points");
export interface RegionInterface {

}
export class Region implements RegionInterface {
    l:Points.Point;
    r:Points.Point;

    constructor(l:Points.Point, r:Points.Point) {
        this.l = l;
        this.r = r;
    }
}