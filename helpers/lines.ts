/// <reference path="../references.ts"/>

import Points = require("points");
import _ = require('lodash');
import fs = require("fs");

    export interface LineInterface {
        getLength(): number;
    }
    export class Line implements LineInterface{
        a: Points.Point;
        b: Points.Point;
        constructor(x: Points.Point, y: Points.Point) {
            this.a = x;
            this.b = y
        }
        getLength() {
            return Math.sqrt(Math.pow((this.a.x - this.b.x), 2) + Math.pow((this.a.y - this.b.y), 2));
        }
        toString(){
            return  "[" + this.a + '<->' + this.b + "]"
        }
    }

    export class Lines {
        lines:Line[];

        constructor(p:Points.Point[]) {

        }
    }
