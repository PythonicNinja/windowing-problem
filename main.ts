import Points = require ("./helpers/points");
import Region = require ("./helpers/region");
import KdTree = require ("./tries/kd_tree");
import Lines = require("./helpers/lines");
import Windowing = require("./windowing/window");

module Search {

    var points = [
        new Points.Point(-1, 0),
        new Points.Point(2, 0),
        new Points.Point(2, 1),
        new Points.Point(2, 3),
        new Points.Point(3, 3),
        new Points.Point(5, 3),
        new Points.Point(-5, 2), //crossing whole region horizontally
        new Points.Point(7, 2),  //
        new Points.Point(0, 1),
        new Points.Point(1, 1),
        new Points.Point(0, 5),
        new Points.Point(5, 5),
        new Points.Point(3, -1),
        new Points.Point(3, -3),
        new Points.Point(-10, 0), //crossing whole region horizontally
        new Points.Point(10, 0),  //
        new Points.Point(1, -5),  // crossing whole region VERTICALLY
        new Points.Point(1, 12)

    ];

    //TODO dodać wsparcie dla linii pionowych przecinających cały region

    var lines = [
        new Lines.Line(points[0], points[1]),
        new Lines.Line(points[2], points[3]),
        new Lines.Line(points[4], points[5]),
        new Lines.Line(points[6], points[7]), //cross
        new Lines.Line(points[8], points[9]),
        new Lines.Line(points[10], points[11]),
        new Lines.Line(points[12], points[13]),
        new Lines.Line(points[14], points[15]), //cross
        new Lines.Line(points[16], points[17])  //cross vertically
    ];


    //Do not change region for this test data
    var search_region = new Region.Region(new Points.Point(-1, 0),
        new Points.Point(5, 10));


    var windowing:Windowing.Window = new Windowing.Window(lines, search_region);

    //Note "belongsToLine" field points on index in array of lines, which is it
    //it's also helpful for some Kd_tree -> windowQuery -> interval tree transitions in parameters
    //it's irrelevant for output
    console.log("windowing results: ");

    var results:Lines.Line[] = windowing.windowQuery(search_region);

    console.log(JSON.stringify(results, null, 3)); //pretty json
    console.log("========================================================================");
    console.log(results); //raw table


    //for(var i=0; i<1000; i++){
    //    if(i==0) var points = [];
    //    points.push(new Points.Point(Math.random()*100, Math.random()*100))
    //}
    //var tree = new KdTree(points, 0);



    //var kd_tree_query = tree.KdTreeQuery(tree.v, search_region);
    //console.log("kd_tree_query",kd_tree_query);
}